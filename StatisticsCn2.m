%--------------------------------------------------------------------------
%DESCRIPTION
%This function calculates Fried number, isoplanatic angle, seeing, and
%scintillation rate from a vertical profile of Cn2. 
%See the following reference for more details: 
%Masciadri et al. (1999). "3D mapping of optical turbulence using an 
%atmospheric numerical model", Astronomy & Astrophysics, 137, 185–202.

%Input: 
%Za:     height above ground level (m) 
%Cn2:    refractive index structure parameter at Za (m^{-2/3})
%Lambda: wavelength (m); e.g., 0.5 micro-m = 500 nm = 0.5e-6 m
%Z_LL:   lower height limit for integration
%Z_UL:   upper height limit for integration

%Output:
%R0:     Fried parameter [cm]
%SNG:    seeing [arcsec]
%IsoP:   isoplanatic angle [arcsec]
%SR:     scintillation rate [unitless]
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%For 500 nm: 
%H/V 5-7:   R0 =  5.0 cm; IsoP =  7.0 murad
%CLEAR-I:   R0 =  5.8 cm; IsoP =  6.7 murad
%SLC:       R0 = 10.0 cm; IsoP = 12.7 murad
%AFGL-AMOS: R0 =  8.9 cm; IsoP = 12.4 murad
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
function [R0,SNG,IsoP,SR] = StatisticsCn2(Z,Cn2,Lambda,Z_LL,Z_UL)

Indx = find(isnan(Cn2) == 1 | Cn2 <= 0); 
Cn2(Indx) = []; Z(Indx) = [];
clear Indx;

Indx = find(Z >= Z_LL & Z <= Z_UL); 
Z    = Z(Indx); 
Cn2  = Cn2(Indx); 
clear Indx;

A    = 0.423*(2*pi/Lambda).^2;
B    = trapz(Z,Cn2); 

%Eq. (19) of Masciadri et al. (1999)
R0   = (A*B)^(-3/5);               
%Eq. (20) of Masciadri et al. (1999)
SNG  = 0.98*Lambda/R0;             

C    = trapz(Z,(Z.^(5/3)).*Cn2);
%Eq. (22) of Masciadri et al. (1999)
H_AO = (C/B)^(3/5);                 

%Eq. (21) of Masciadri et al. (1999)
IsoP = 0.31*R0/H_AO;                

D    = trapz(Z,(Z.^(5/6)).*Cn2);
%Eq. (24) of Masciadri et al. (1999)
SR   = 19.12*(Lambda^(-7/6)).*D;    
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%Convert units
R0   = R0*100;              %m to cm
SNG  = SNG/(4.84813e-6);    %rad to arcsec
IsoP = IsoP/(4.84813e-6);   %rad to arcsec
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Copyright, 2015, Sukanta Basu
%
% The MIT License (MIT)
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%--------------------------------------------------------------------------
