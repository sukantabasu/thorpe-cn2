%--------------------------------------------------------------------------
%DESCRIPTION
%This function generates vertical profiles of Cn2. These climatological 
%models only depend on height. See the following reference for more details: 
%R. R. Beland, Infrared and Electro-Optical Systems Handbook, 
%Vol. 2 (SPIE, 1993).

%Input: 
%HGT:   terrain height (m) 
%opt:   option for a specific traditional Cn2 model
%       1: SLC (Submarine Laser Communications) nighttime model        
%       2: SLC daytime model
%       3: Hufnagel-Valley (H-V) 5/7 model
%       4: AFGL-AMOS nighttime model
%       5: CLEAR-1 nighttime model 

%Output:
%Za:    height (above ground level; m)
%Zm:    height (mean sea level; m)
%Cn2:   refractive index structure parameter at Z (m^{-2/3})
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
function [Za,Zm,Cn2] = ClimatologicalCn2(opt,HGT)

%--------------------------------------------------------------------------
zmin = 10;          %minimum height (m) 
zmax = 30000;       %maximum height (m)
dz   = 10;          %inrement of height (m)
Z    = zmin:dz:zmax; 
Nz   = numel(Z);    %number of vertical levels

Za   = Z;           %height above ground level (m)
Zm   = Z + HGT;     %height above mean sea level (m)    
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
Cn2 = zeros(size(Z));

%SLC nighttime model
if opt == 1 
    for k = 1:Nz
        if Z(k) <= 18.5
            Cn2(k) = 8.40e-15;
        elseif Z(k) > 18.5 && Z(k) <= 110
            Cn2(k) = 2.87e-12*(Z(k).^(-2));
        elseif Z(k) > 110 && Z(k) <= 1500
            Cn2(k) = 2.5e-16;
        elseif Z(k) > 1500 && Z(k) <= 7200
            Cn2(k) = 8.87e-7*(Z(k).^(-3));
        elseif Z(k) > 7200 && Z(k) <= 20000
            Cn2(k) = 2.00e-16*(Z(k).^(-0.5));
        else
            Cn2(k) = NaN;
        end
    end
    
    
%SLC daytime model    
elseif opt == 2 
    for k = 1:length(Z)
        if Z(k) <= 18.5
            Cn2(k) = 1.70e-14;
        elseif Z(k) > 18.5 && Z(k) <= 240
            Cn2(k) = 3.13e-13*(Z(k).^(-1));
        elseif Z(k) > 240 && Z(k) <= 880
            Cn2(k) = 1.30e-15;
        elseif Z(k) > 880 && Z(k) <= 7200
            Cn2(k) = 8.87e-7*(Z(k).^(-3));
        elseif Z(k) > 7200 && Z(k) <= 20000
            Cn2(k) =  2.00e-16*(Z(k).^(-0.5));
        else
            Cn2(k) = NaN;
        end
    end

%H-V 5/7 model    
elseif opt == 3 
    h   = Z/1000; 
    W   = 21; 
    A   = 1.7e-14;
    Cn2 = 8.2*10^(-26) * W^2 * (h.^10) .* exp(-h) + 2.7* 1e-16 * exp(-h/1.5) + A*exp(-h/0.1);
    
%AFGL-AMOS nighttime model    
elseif opt == 4 
    %NOTE: this model assumes height is from mean sea level and in km
    h = (Z+HGT)/1000; 
    for k = 1:length(h)
        if h(k) <= 3.052
            Cn2(k) = NaN;
        elseif h(k) > 3.052 && h(k) <= 5.2
            Cn2(k) = 10.^(-12.412 -0.4713*h(k) -0.0906*h(k)^2);
        elseif h(k) > 5.2 && h(k) <= 30
            Cn2(k) = 10.^(-17.1273 -0.0301*h(k) -0.0010*h(k)^2 + 0.5061*exp(-0.5*((h(k)-15.0866)/3.2977)^2));
        else
            Cn2(k) = NaN;
        end
    end
    
%CLEAR I nighttime model 
elseif opt == 5 
    %NOTE: this model assumes height is from mean sea level and in km
    h = (Z+HGT)/1000; 
    for k = 1:length(h)
        if h(k) <= 1.23
            Cn2(k) = NaN; 
        elseif h(k) > 1.23 && h(k) <= 2.13
            Cn2(k) = 10.^(-10.7025 -4.3507*h(k) + 0.8141*h(k)^2);
        elseif h(k) > 2.13 && h(k) <= 10.34
            Cn2(k) = 10.^(-16.2897 +0.0335*h(k) - 0.0134*h(k)^2);
        elseif h(k) > 10.34 && h(k) <= 30
            Cn2(k) = 10.^(-17.0577 -0.0449*h(k) -0.0005*h(k)^2 + 0.6181*exp(-0.5*((h(k)-15.5617)/3.4666)^2));
        else
            Cn2(k) = NaN;
        end
    end
    
end
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Copyright, 2015, Sukanta Basu
%
% The MIT License (MIT)
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%--------------------------------------------------------------------------