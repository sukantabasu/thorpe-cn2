%--------------------------------------------------------------------------
%DESCRIPTION
%This function computes CT2 using the so-called Thorpe scale approach; see
%Eq. 3 of Basu (2015):
%CT2  = c1 * L_T^(4/3) * (dTh_s/dz)^2
%CT2  = temperature structure parameter (K^2 m^{-2/3})
%L_T  = Thorpe scale (m)
%Th_s = sorted potential temperature (K)
%c1   = a constant; observational data from Hawaii 2002 field campaign
%       indicated c1 = 0.02 
%Once CT2 is computed, Cn2 can be estimated by the well-known Gladstone's
%relationship; see Eq. 2 of Basu (2015)

%For a detailed description of the proposed approach, please refer to:
%Basu, S. (2015). "A simple approach for estimating the refractive index
%structure parameter (Cn2) profile in the atmosphere", Optics Letters,
%40(17), 4130-4133.

%Input:
%Z       = heights above ground level (m); can be non-uniformly spaced
%Th      = potential temperature (K) at Z
%F       = filter width for moving averaging
%gap_opt = 0: no gap filling; 1: use robust regression-based gap filling

%Gap filling: Basu (2015) found that L_T and dTh_s/dz values are inversely
%correlated. A power-law (robust) regression is then used to extrapolate
%the L_T values for strongly stratified conditions.

%Output:
%L_T     = Thorpe scale (m)
%CT2     = temprature structure parameter (K^2 m^{-2/3}) at Z

%Example:
%
%--------------------------------------------------------------------------

function [CT2,L_T] = ThorpeCT2(Z,TH,F,gap_opt)

%Sort TH profile and compute Thorpe scale----------------------------------
[THx,Ix]  = BubbleSort(TH);

DISP1     = Z - Z(Ix);  %Displacement with respect to new location
DISP2     = zeros(size(DISP1));
DISP2(Ix) = DISP1;      %Displacement with respect to original location

L_T       = abs(DISP2);

%Smooth L_T profiles (removes zero values)
L_T       = smooth(L_T,F);
%--------------------------------------------------------------------------

%Compute gradient of sorted potential temperature profiles-----------------
dTHdz     = Gradient_Nonuniform(Z,THx);
dTHdz     = smooth(dTHdz,F);
dTHdz     = interp1(Z(2:end-1),dTHdz,Z,'linear','extrap');
%Make sure that the gradient is not negative in a sorted profile (could be
%an artifact of smoothing and/or interpolation)
III       = dTHdz < 0; 
dTHdz(III)= NaN;
clear III;
%--------------------------------------------------------------------------

%Perform robust regression and gap filling---------------------------------
dzm       = median(diff(Z));
Indx1     = find(L_T < dzm);

if gap_opt == 0
    L_T(Indx1) = NaN;
else
    Indx2      = find(L_T >= dzm);
    p          = robustfit(log(dTHdz(Indx2)),log(L_T(Indx2)));
    L_T(Indx1) = exp(p(2)*log(dTHdz(Indx1)) + p(1));
    clear Indx2; 
end
clear Indx1;
%--------------------------------------------------------------------------

%Compute temperature structure parameter-----------------------------------
c1   = 0.02;
CT2  = c1*(L_T.^(4/3)).*(dTHdz.^2);
%--------------------------------------------------------------------------

return

%--------------------------------------------------------------------------
%Compute vertical gradients with central differencing approach. This
%function works for non-uniform grid spacing. Reference: Hoffmann and
%Chiang (2000): Computational Fluid Dynamics, vol. 1. (see Example 2.5 on
%page 45).

function dydx = Gradient_Nonuniform(x,y)

N  = length(x);
f1 = x(2:N-1) - x(1:N-2);
f2 = x(3:N) - x(2:N-1);
a  = f2./f1;

Num = y(3:N) + (a.^2 - 1).*y(2:N-1) - a.^2.*y(1:N-2);
Den = a.*(a+1).*f1;

dydx = Num./Den;

return
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
%Bubble sort algorithm. Why do we need it?
%According to Stips (2005): "For practical purposes it is important to use
%a so-called bubble-sort algorithm, because this algorithm will not resort
%two neighboring equal densities. The so-called quick-sort algorithm, which
%is faster, could exchange equal-density samples, thus creating Thorpe
%dispacements in a neutral density profile."

%Input:
%X       = unsorted series (e.g., vertical potential temperature profile)

%Output:
%X       = sorted series
%iX      = sort index which specifies how the elements of the unsorted
%          profile were rearranged to obtain the sorted profile

function [X,iX] = BubbleSort(X)

N  = numel(X);
iX = 1:N;

for pass = 1:10^10 %maximum number of passes is set as a large number
    cnt = 0;
    for i = 2:N
        if X(i) < X(i-1)
            dum    = X(i-1);
            X(i-1) = X(i);
            X(i)   = dum;
            
            dum    = iX(i-1);
            iX(i-1)= iX(i);
            iX(i)  = dum;
            
            cnt    = cnt+1;
        end
    end
    if cnt == 0
        break;
    end
end

return
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Copyright, 2015, Sukanta Basu
%
% The MIT License (MIT)
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%--------------------------------------------------------------------------